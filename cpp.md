# C++ Standards for SurfLab

This section contains all coding style guidelines for SurfLab C++ projects.  It is organized by Variables, Classes, Functions, and Other Conventions.

Special emphasis is given to the cardinal rules: 

* **Each statement shall always be in a separate line, however small it may appear**.

```javascript
int t_One = 0; int tTwo = 1;  // BAD!

int t_One = 0;                // GOOD!
int t_Two = 1;                // GOOD!
```

* **File Names**

File names shall always be in [upper camel case](http://wiki.c2.com/?LowerCamelCase). For instance, the following file name `MyFileName.hpp` is acceptable.

# Variables

The following guidelines apply to variables.

## Naming Variables

Use clear and concise names for variables.  When variables have names with multiple words, use [lower camel case](https://en.wikipedia.org/wiki/Camel_case) to distinguish each word, e.g. `aUserId`. Use the proper prefix for a variable (see Prefixes). Finally, common words should be abbreviated (see Abbreviations).

## Prefixes

When naming variables, use the following prefixes depending on the type of variable:

* `Member variable`: m_ - (e.g., m_Vals, m_NumRows)
* `Argument variable`: a_ - (e.g., a_Vals, a_InputParameter)
* `Temporary variable`: t_ - (e.g., t_Vector)

## Suffixes

When naming variables, use the following suffixes depending on the type of variable:

- `Iterator`: _it - (e.g., t_cell_it, t_vertex_it)

## Abbreviations

The following abbreviations are approved:

* `cpts` - control points
* `fcn` - function
* `vert` - vertex
* `num` or `Num` - number (i.e. numNodes, tNumNodes)

## Declaring Variables

When declaring variables, use the following guidelines:

* Declare and initialize one variable at a time. 

```javascript
int a_AnInteger, anotherInteger // BAD!
  
int a_AnInteger                 // BAD!
int a_AnotherInteger
  
int    a_AnInteger  = 1;        // GOOD!
double a_RealNumber = 5.0;

int a_AnInteger = 1;            // GOOD!
double a_RealNumber = 5.0;
```

* The characters `*` and `& ` should be written together with the types of variables instead of with the names of variables in order to emphasize that they are part of the type definition.

```javascript
int *a_Integer    // BAD!
int* a_IntPointer // GOOD!
```

# Classes

The following guidelines apply to classes.

## Class Name

Class names should always use [upper camel case](https://en.wikipedia.org/wiki/Camel_case), e.g. `MyClass`. Further, if a class name has more than one word, use [upper camel case](https://en.wikipedia.org/wiki/Camel_case) and **Do Not** separate the names with underscores.

```
My_Class_Name  // BAD!
MyClassName    // GOOD!
```

# Functions

The following guidelines apply to functions.

**NOTE** - `Constructors` (and `destructors`) are the exception to this rule since C++ requires all `constructor` and `destructors` to have the same name as the class name.

## Function Names

Class function names should have [lower camel case](http://wiki.c2.com/?LowerCamelCase) names. If a function name includes multiple words, do not separate the names with underscores.

```
my_class_function_name()   // BAD!
myClassFunctionName()      // GOOD!
```

**Non-member function names are an exception to this rule**. A non-member function is a function that is not defined inside a class, e.g. inline function or helpers in separate file. Non-member functions should have lower case names, e.g. 

```
Function()   // BAD!
function()   // GOOD!
```

**If the non-member function name has multiple words**, separate each name with an underscore as follows

```
MyFreeFunctionName()      // BAD!
myFreeFunctionName()      // BAD!
myfreefunctionname()      // BAD!
my_Free_Function_Name()   // BAD!
My_Free_Function_Name()   // BAD!

my_free_function_name()   // GOOD!
```

# Comments

Place a comment describing the input and output of a function immediately before declaration.  If appropriate, also describe briefly how the function works.

```c++
/*
 *  Input:  two arrays containing numeric data.
 *		    The arrays must be of the same length
 *		    and should contain the same type of data
 *	Output: the dot product of the two arrays
 *
 *	Iterates through the arrays, summing the 
 *	product of the elements at the same indices.
 */
template<typename T, size_t S>
double dot(const std::array<T, S> a_Vec1, const std::array<T, S> a_Vec2)
{
    double t_Result=0.0;
    for(int t_Index=0; t_Index<S; ++t_Index){
        t_Result += a_Vec1[t_Index]*a_Vec2[t_Index];
    }
    return t_Result;
}
```



# Other Conventions

## Operator Spacing

[C++ operators](https://www.geeksforgeeks.org/operators-c-c/) should be spaced as follows:

* Always use spaces before and after the following operators: =, +, -, *, and all logical operators.
* Do not use spaces around �.� or �->', nor between unary operators and operands.

```javascript
i++    // this is GOOD!
i ++   // this is BAD!
 
AFullArray.AMemberFunction     // this is GOOD!
AFullArray . AMemberFunction   // this is BAD!
 
this->MemberFunction     // GOOD!
this -> MemberFunction   // BAD!
```

## Switch Statements

A switch statement must always contain a default branch use to handle unexpected cases.

```javascript
switch (VariableName)
{
case ACASE:
{
    // lines of code
    break;
}
case ANOTHERCASE:
{
    // lines of code
    break;
}
default: // this is necessary
{
    // this is the default case
}
} // end of switch structure
```

## Headers

Never use `using` in a header file.

Always use include guards in header files.